package miTest;
import miCalculadora.*;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class TestSumaYResta {
	Calculadora c;
	
	@Before
	public void setUp() throws Exception {
		c=new Calculadora();
	}

	@Test
	public void testSuma() {
		assertTrue(c.suma(2, 3)==5);
		}

}
